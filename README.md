# Content Moderation Best Practices
## Content policies

- **Use understandable policies.** 
This applies to both the policies you publish externally and the more detailed, execution-focused version of these policies that help your moderators make informed and consistent decisions. While the decisions and trade-offs underlying these policies are likely complex, once resolution is reached the policies themselves need to be expressed in simple terms so that users can easily understand community guidelines and moderators can more easily recognize violations. When the rules aren’t clear, two problems arise: (i) moderators may have to rely on gut instincts rather than process, which can lead to inconsistency; and (ii) users lose trust because policies appear arbitrary. Consider providing examples of acceptable and unacceptable behaviors to help both users and moderators see the application of your policies in action (many examples will be more clarifying than just a few). Again, this is not to say that creating policies is an easy process, there will be many edge cases that make this process challenging. We touch more on this below.

- **Publicize policies and changes.** 
Don’t pull the rug out from under your users. Post policies in an easy-to-find place, and notify users when they change. How to accomplish the latter will depend on your audience, but you should make a good faith effort to reach them. For some, this may mean emailing; for others, a post pinned to the top of a message board will suffice. In any case, avoid placing update notifications anywhere that could be perceived as burying them. The last thing you want is to appear as if you have something to hide.


- **Build policies on top of data.**
The time may come when your policies will be called into question. This is exactly the time when you want to be able to present a thoughtful approach to their creation and maintenance. Policies based on intuition or haphazard responses to problems, while possibly quicker, will likely cause more issues in the long run. Grounding your content policies on solid facts will make your community a healthier, more equitable place for users.


- **Iterate.** 
Times change, and what works when you start your community won’t necessarily work as it grows. For instance, new vocabulary may come into play, and slurs can be reappropriated by marginalized groups as counterspeech. This can also be a great opportunity to solicit feedback from your community to both inform changes and more deeply engage users. And keep in mind that change need not be disruptive — communities can absorb lots of small, incremental changes or clarifications to policies. The real danger comes with sweeping changes that significantly alter your stances, which can create confusion or anger, and lead to unexpected consequences.

## Harassment and abuse detection


- **Be proactive.**
Addressing abusive content after it’s been posted generally only serves to highlight flaws and omissions in your policies. Beyond that, it puts the onus of moderation on users, who may find themselves in the position of having to inform you of problematic content.
Proactive moderation can make use of automated initial detection and human moderators working in concert. Automated systems can flag potentially abusive content, after which human moderators with a more nuanced understanding of your community can jump in to make a final call.

- **Factor in context.**
Words or phrases that are toxic in one setting — one user directing a slur at another — may not be in another — a user reclaiming a slur or merely discussing its use. Simple mechanisms like word filters and pattern matching are inadequate for this task, as they tend to under-censor harmful content and over-censor non-abusive content. Again, having a policy and systems that can negotiate these kinds of nuances is critical to maintaining a platform’s health.

- **Create a scalable foundation.**
Relying on human moderation and sparse policies may work when your goal is to get up and running, but can create problems down the road. As communities grow, the complexity of expression and behavior grows exponentially. Establishing policies that can handle increased scale and complexity over time can save time and money — and prevent abuses — in the long term.

- **Brace for abuse.**
There’s always the danger of persistent bad actors poisoning the well for an entire community. In many ways, their actions are like those of traditional hackers in pursuit of exploits. They may repeatedly test keyword dictionaries to find gaps, or manipulate naive machine learning-based systems to “pollute the well.” Investing in industrial-grade detection tooling early on is the most effective way to head off these kinds of attacks.

- **Assess effectiveness.**
No system is infallible, so you’ll need to build regular evaluations of your moderation system into your processes. Doing so will help you understand whether a given type of content is being identified correctly or incorrectly — or missed entirely. That last part is perhaps the biggest problem you’ll face. We recommend using production data to build evaluation sets, allowing you to track performance over time.
## Moderation actions

- **Act swiftly.**
Time is of the essence — the longer an offensive post remains, the more harm can come to your users and your community’s reputation. Inaction or delayed response can create the perception that your platform tolerates hateful or harassing content. And, unfortunately, a deterioration of user trust will follow right behind.

- **Give the benefit of the doubt.**
From time to time, even “good” community members may unintentionally post hurtful content. That’s why, when communicating with users about flags, it’s important to provide ample notice of disciplinary actions like suspensions. Doing so will allow well-intentioned users to course-correct, and, in the case of malicious users, provide a solid basis for more aggressive measures in the future.

- **Embrace transparency.**
One of the biggest risks in taking action against a community member is the chance you’ll come across as capricious or unjustified. Regularly reporting anonymized, aggregated moderation actions will foster a feeling of safety among your user base.

- **Prepare for edge cases.**
Just as you can’t always anticipate new terminology, there will likely be incidents your policies don’t clearly cover. One recommendation for handling these types of hiccups is a process that triggers the use of an arbiter that holds final authority.
Another method is to imagine the content or behavior to be 10,000 times as common as it is today. The action you would take in that scenario can inform the action you take today. Regardless of the system you develop, be sure to document all conversations, debate, and decisions. And once you’ve reached a decision, formalize it by updating your content policy.

- **Respond appropriately.**
We’ve found that only a small portion of toxic content comes from persistent, determined bad actors. The majority of incidents are due to regular users having an off-day. That’s why it’s important to not apply draconian measures like permanent bans at the drop of a hat. Lighter measures like email or in-app warnings, content removal, and temporary bans send a clear signal about unacceptable behavior while allowing users to learn from their mistakes.
There are a multitude of options you can consider when responding to an incident. For a list of possible remedies, see the Taxonomy of Remedy Options chart at the bottom of our blog post. 

- **Target remedies.**
Depending on the depth of your community, a violation may be limited to a subgroup within a larger group. Be sure to focus on the problematic subgroup to avoid disrupting the higher-level group.

- **Create an appeals process.**
Even as systems become more sophisticated, false positives are bound to pop up from time to time. So creating an equitable structure that allows users to appeal when they believe they’ve been wrongly moderated is important for establishing and building trust. As with other parts of your policies, transparency plays a big role. The more effort you put into explaining and publicizing your appeals policy up front, the safer and stronger your community will be in the long run.

- **Protect moderators.** While online moderation is a relatively new — and poorly understood — field, the stresses it causes are very real. Focusing on the worst parts of a platform can be taxing psychologically and emotionally. Support for your moderators in the form of removing daily quotas, enforcing break times, and providing counseling is good for your community — and the ethical thing to do.
And if you’re considering opening a direct channel for users to communicate with Trust & Safety agents, be aware of the risks. While it can help dissipate heightened reactions from recently censored users, protecting moderators here, too, is critical. Use shared, monitored inboxes for inbound messages and anonymized handles for employee accounts. Use data to understand which moderators on your team are exposed to certain categories or critical levels of abusive content. And as an extra layer of protection, provide employees with personal online privacy-protecting solutions such as DeleteMe.
## Measurement

- **Maintain logs.** Paper trails are more than just a way to keep everyone informed about an active incident. They also serve as invaluable reference material. Be sure to keep complete records of flagged content. This includes the content under consideration, associated user or forum data, justification for the flag, moderation decisions, and post mortem notes, when available. This information can help inform future moderation debates and identify inconsistencies in the application of your policies.

- **Use metrics.** Moderation is possibly the single most impactful determinant of a community user’s experience. Measurement of its effectiveness should be subject to the same rigor you’d apply to any other part of your product. By evaluating your moderation process with both quantitative and qualitative data, you’ll gain insight into user engagement, community health, and the impact of toxic behavior.
- **Use feedback loops.** A final decision on a content incident need not be the end of the line. Don’t let the data you’ve collected through the process go to waste. Make it a part of regular re-evaluations and updates of content policies to not only save effort on similar incidents, but also to reinforce consistency.
